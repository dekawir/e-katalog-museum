@extends('layouts.index')
@section('content')
<section class="section">
  <div class="row">
    <div class="col-lg-2">
      <div class="card">
        <!-- Large Modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#largeModal">
          Tambah
        </button>

        <div class="modal fade" id="largeModal" tabindex="-1">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Tambah {{ $title }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <form method="POST" enctype="multipart/form-data">
                  @csrf
                  <!-- General Form Elements -->
                
                    <div class="row mb-3">
                      <label for="inputText" class="col-sm-2 col-form-label">Nama</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" value="{{ old('nama') }}">
                      </div>
                    </div>
                    <div class="row mb-3">
                      <label for="inputText" class="col-sm-2 col-form-label">Deskripsi</label>
                      <div class="col-sm-10">
                        <textarea name="deskripsi" id="" cols="72" rows="10">{{ old('deskripsi') }}</textarea>
                      </div>
                    </div>
                   
                    <div class="row mb-3">
                      <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="file" name="images[]">
                      </div>
                    </div>
                    {{-- <div class="row mb-3">
                      <label class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-10">
                        <select class="form-select" aria-label="Default select example" name="status">
                          <option>Pilih Status</option>
                          <option value="Aktif" value="{{ old('nama')=='Aktif'?'selected':'' }}">Aktif</option>
                          <option value="Tidak Aktif" value="{{ old('nama')=='Tidak Aktif'?'selected':'' }}">Tidak Aktif</option>
                        </select>
                      </div>
                    </div> --}}

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" name="add" value="add" class="btn btn-primary">Save changes</button>
                  </div>
                </form>
            </div>
          </div>
        </div><!-- End Large Modal-->
        </div><!-- End Large Modal-->

      </div>
    </div>
  </div>
  <div class="row">
    <section class="section contact">

        <div class="row gy-4">
  
          <div class="col-xl-12">
  
            <div class="row">
            {{-- @if (!empty($lantai)) --}}
                
            @foreach ($lantai as $i)
                
              <div class="col-lg-6">
                <div class="info-box card">
                    <div class="d-flex align-items-center">
                        <div class="col-sm-4">
                            <i class="ri-building-line"></i>
                            <h2>{{ $i['nama'] }}</h2>
                            <p>{{ $i['deskripsi'] }}</p>
                            <button onclick="editbtn({{ $i['id'] }})" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editLargeModal"><i class="bi bi-pencil-square"></i></button>
                            <button onclick="del({{ $i['id'] }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button>
                        </div>
                        <div class="col-lg-8">
                            <img src="{{ asset('images/data jadwal/'.$i['image'].' ') }}" class="d-block w-100" alt="...">                
                        </div>
                    </div>
                </div>
              </div>

            @endforeach
            {{-- @endif --}}

            </div>
  
          </div>
    
        </div>
  
      </section>

  </div>
</section>

<div class="modal fade" id="editLargeModal" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah {{ $title }}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <!-- General Form Elements -->
      <form method="POST" enctype="multipart/form-data">
        @csrf
        <!-- General Form Elements -->
          <input type="hidden" name="id" id="id">
          <div class="row mb-3">
            <label for="inputText" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}">
            </div>
          </div>
          <div class="row mb-3">
            <label for="inputText" class="col-sm-2 col-form-label">Deskripsi</label>
            <div class="col-sm-10">
              <textarea name="deskripsi" id="deskripsi" cols="72" rows="10">{{ old('deskripsi') }}</textarea>
            </div>
          </div>
         
          <div class="row mb-3">
            <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
            <div class="col-sm-10">
              <input class="form-control" type="file" name="images[]">
            </div>
          </div>
          {{-- <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Status</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select example" name="status">
                <option>Pilih Status</option>
                <option value="Aktif" value="{{ old('nama')=='Aktif'?'selected':'' }}">Aktif</option>
                <option value="Tidak Aktif" value="{{ old('nama')=='Tidak Aktif'?'selected':'' }}">Tidak Aktif</option>
              </select>
            </div>
          </div> --}}

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" name="edit" value="edit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
      
    </div>
  </div>
</div><!-- End Large Modal-->

<!-- Basic Modal -->

<div class="modal fade" id="basicModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Apakah anda yakin menghapus ini?
      </div>
      <form method="POST" action="{{ route('del.jadwal') }}">
        @csrf
        <input type="hidden" name="delId" id="delId">
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" name="del" value="del" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div><!-- End Basic Modal-->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
  function editbtn(id){
      // var input = document.getElementById("editId");
      // input.value = id;

      // console.log(id)
      
      $.ajax({
          type: "GET",
          url: "/data-jadwal/"+id,
          success: function (response) {
              console.log(response.data);
              $('#id').val(response.data.id).change();
              $('#nama').val(response.data.nama).change();
              $('#deskripsi').val(response.data.deskripsi).change();
             
             
          }
      });
  }
</script>
<script>
function del(id){
    var input = document.getElementById("delId");
    input.value = id;
    console.log(id);

}
</script>
@endsection