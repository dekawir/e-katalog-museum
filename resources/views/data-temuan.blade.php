@extends('layouts.index')
@section('content')
    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{{ $title }}</h5>
              <!-- Large Modal -->
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#largeModal">
                Tambah
              </button>

              <div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Tambah {{ $title }}</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <!-- General Form Elements -->
                    <form method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="row mb-3">
                        <label class="col-sm-2 col-form-label">Nama Benda</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="{{ old('nama') }}" name="nama" >
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label  class="col-sm-2 col-form-label">Penemu</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="penemu" value="{{ old('penemu') }}">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label class="col-sm-2 col-form-label">Jenis Benda</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="jenis_benda" value="{{ old('jenis_benda') }}">
                        </div>
                      </div>
                        <div class="row mb-3">
                            <label for="inputDate" class="col-sm-2 col-form-label">Tgl. Ditemukan</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" name="tgl_ditemukan" value="{{ old('tgl_ditemukan') }}">
                            </div>
                        </div>
                      <div class="row mb-3">
                        <label  class="col-sm-2 col-form-label">Lokasi</label>
                        <div class="col-sm-10">
                          <textarea id="" cols="72" rows="5" name="lokasi">{{ old('lokasi') }}</textarea>
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
                        <div class="col-sm-10">
                        <input class="form-control" type="file" name="images[]" multiple>
                        </div>
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" value="add" name="add">Save changes</button>
                    </div>
                  </form><!-- End General Form Elements -->

                  </div>
                </div>
              </div><!-- End Large Modal-->

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Benda</th>
                    <th scope="col">Penemu</th>
                    <th scope="col">Jenis Benda</th>
                    <th scope="col">Tgl. Ditemukan</th>
                    <th scope="col">Lokasi</th>
                    <th scope="col">Image</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                      $no=1
                  @endphp
                  @foreach ($data as $i)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $i->nama }}</td>
                    <td>{{ $i->penemu }}</td>
                    <td>{{ $i->jenis_benda }}</td>
                    <td>{{ date('d-F-Y', strtotime($i->tgl_ditemukan)) }}</td>
                    <td>{{ $i->lokasi }}</td>
                    <td> <img height="100" src="{{ asset('images/data temuan/'.$i->image.' ') }}" alt=""> </td>
                    <td>
                      
                      <button onclick="editbtn({{ $i->id }})" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editLargeModal"><i class="bi bi-pencil-square"></i></button>
                      <a class="btn btn-warning" href="{{ URL::to('data-temuan-details/'.$i->id.' ') }}"><i class="bi bi-search"></i></a>
                      <button onclick="del({{ $i->id }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button>
                     
                      {{-- <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a> --}}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

    <div class="modal fade" id="editLargeModal" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Tambah {{ $title }}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
          <!-- General Form Elements -->
          <form method="POST">
            @csrf
            <input type="hidden" name="id" id="id">
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Nama Benda</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" value="{{ old('nama') }}" name="nama"  id="nama">
                </div>
              </div>
              <div class="row mb-3">
                <label  class="col-sm-2 col-form-label">Penemu</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="penemu" value="{{ old('penemu') }}" id="penemu">
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Jenis Benda</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="jenis_benda" value="{{ old('jenis_benda') }}" id="jenis_benda">
                </div>
              </div>
                <div class="row mb-3">
                    <label for="inputDate" class="col-sm-2 col-form-label">Tgl. Ditemukan</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="tgl_ditemukan" value="{{ old('tgl_ditemukan') }}" id="tgl_ditemukan">
                    </div>
                </div>
              <div class="row mb-3">
                <label  class="col-sm-2 col-form-label">Lokasi</label>
                <div class="col-sm-10">
                  <textarea id="lokasi" cols="72" rows="5" name="lokasi">{{ old('lokasi') }}</textarea>
                </div>
              </div>            

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" value="edit" name="edit">Save changes</button>
          </div>
        </form><!-- End General Form Elements -->

        </div>
      </div>
    </div><!-- End Large Modal-->


    <!-- Basic Modal -->
   
    <div class="modal fade" id="basicModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Konfirmasi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Apakah anda yakin menghapus ini?
          </div>
          <form method="POST" action="{{ route('del.temuan') }}">
            @csrf
            <input type="hidden" name="delId" id="delId">
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" name="del" value="del" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- End Basic Modal-->


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
      function editbtn(id){
          // var input = document.getElementById("editId");
          // input.value = id;
  
          // console.log(id)
          
          $.ajax({
              type: "GET",
              url: "/data-temuan-edit/"+id,
              success: function (response) {
                  console.log(response.data);
                  $('#id').val(response.data.id).change();
                  $('#nama').val(response.data.nama).change();
                  $('#penemu').val(response.data.penemu).change();
                  $('#jenis_benda').val(response.data.jenis_benda).change();
                  $('#tgl_ditemukan').val(response.data.tgl_ditemukan).change();
                  $('#lokasi').val(response.data.lokasi).change();
                 
              }
          });
      }
  </script>
  <script>
    function del(id){
        var input = document.getElementById("delId");
        input.value = id;
        console.log(id);

    }
</script>


@endsection