@extends('layouts.index')
@section('content')
<section class="section">
@foreach ($b as $i)
    
<div class="row">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{ $title }}</h5>

            <form method="POST" enctype="multipart/form-data">
                @csrf
                <!-- General Form Elements -->
                <div class="row mb-3">
                    <label for="inputText" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama" value="{{ $i['nama'] }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputText" class="col-sm-2 col-form-label">Deskripsi</label>
                    <div class="col-sm-10">
                    <textarea name="deskripsi" id="" cols="125" rows="10">{{ $i['deskripsi'] }}</textarea>
                    </div>
                </div>
                
                {{-- <div class="row mb-3">
                    <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
                    <div class="col-sm-10">
                    <input class="form-control" type="file" name="images[]" multiple>
                    </div>
                </div> --}}
                
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                    <select class="form-select" aria-label="Default select example" name="status">
                        <option value="Aktif" {{ $i['status']=='Aktif'?'selected':'' }}>Aktif</option>
                        <option value="Tidak Aktif" {{ $i['status']=='Tidak Aktif'?'selected':'' }}>Tidak Aktif</option>
                    </select>
                    </div>
                </div>

                
                <div class="modal-footer">
                <button type="submit" name="edit" value="edit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{ $title }} Gambar</h5>
           
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#largeModal">
            Tambah
            </button>
          
            <!-- Large Modal -->
            
    
            <div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah {{ $title }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" enctype="multipart/form-data">
                            @csrf
                            <!-- General Form Elements -->
                            <div class="row mb-3">
                                <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="file" name="images[]" multiple>
                                </div>
                                </div>        
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" name="add_gambar" value="add_gambar" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- End Large Modal-->
            <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Terakhir Ditambahkan</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                        $no=1;
                    @endphp
                    @foreach ($i['images'] as $g)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td><img src="{{ asset('images/benda sejarah/'.$g->image.' ') }}" width="100" alt="..."></td>
                        <td>{{ $g->created_at }}</td>
                        <td><button onclick="del({{ $g->id }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endforeach

</section>

<!-- Basic Modal -->

<div class="modal fade" id="basicModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Konfirmasi</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          Apakah anda yakin menghapus ini?
        </div>
        <form method="POST" action="{{ route('del.gambar') }}">
          @csrf
          <input type="hidden" name="delId" id="delId">
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" name="del" value="del" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div><!-- End Basic Modal-->
  
  
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  
  <script>
  function del(id){
      var input = document.getElementById("delId");
      input.value = id;
      console.log(id);
  
  }
  </script>



@endsection