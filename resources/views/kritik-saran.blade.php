@extends('layouts.index')
@section('content')
    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{{ $title }}</h5>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">No. Telp</th>
                    <th scope="col">Kritik & Saran</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                      $no=1
                  @endphp
                  @foreach ($data as $i)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ date('d-F-Y',strtotime($i->created_at)) }}</td>
                    <td>{{ $i->no_telp }}</td>
                    <td>{{ $i->kritik_saran }}</td>
                    <td>
                      
                      {{-- <button onclick="editbtn({{ $i->id }})" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editLargeModal"><i class="bi bi-pencil-square"></i></button>
                      <button onclick="del({{ $i->id }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button>
                      --}}
                      {{-- <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a> --}}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

    <div class="modal fade" id="editLargeModal" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Tambah {{ $title }}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
          <!-- General Form Elements -->
          <form method="POST">
            @csrf
            <input type="hidden" name="id" id="id">
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="nama" value="{{ old('nama') }}" name="nama" >
              </div>
            </div>
            <div class="row mb-3">
              <label  class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" name="password">
                <span>Kosongkan jika tidak mengubah password!</span>
              </div>
            </div>
            <div class="row mb-3">
              <label  class="col-sm-2 col-form-label">Alamat</label>
              <div class="col-sm-10">
                <textarea cols="72" rows="5" id="alamat" name="alamat">{{ old('alamat') }}</textarea>
              </div>
            </div>
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">No. Telp</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="no_telp" onkeypress="return hanyaAngka(event)" name="no_telp" value="{{ old('no_telp') }}">
              </div>
            </div>
            

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" value="edit" name="edit">Save changes</button>
          </div>
        </form><!-- End General Form Elements -->

        </div>
      </div>
    </div><!-- End Large Modal-->


    <!-- Basic Modal -->
   
    <div class="modal fade" id="basicModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Konfirmasi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Apakah anda yakin menghapus ini?
          </div>
          <form method="POST" action="{{ route('del.pengunjung') }}">
            @csrf
            <input type="hidden" name="delId" id="delId">
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" name="del" value="del" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- End Basic Modal-->


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
      function editbtn(id){
          // var input = document.getElementById("editId");
          // input.value = id;
  
          // console.log(id)
          
          $.ajax({
              type: "GET",
              url: "/data-pengunjung-edit/"+id,
              success: function (response) {
                  console.log(response.data);
                  $('#id').val(response.data.id).change();
                  $('#nama').val(response.data.nama).change();
                  $('#email').val(response.data.email).change();
                  $('#alamat').val(response.data.alamat).change();
                  $('#no_telp').val(response.data.no_telp).change();
                 
              }
          });
      }
  </script>
  <script>
    function del(id){
        var input = document.getElementById("delId");
        input.value = id;
        console.log(id);

    }
</script>


@endsection