@extends('layouts.index')
@section('content')
<section class="section">
  <div class="row">
    <div class="col-lg-2">
      <div class="card">
        <!-- Large Modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#largeModal">
          Tambah
        </button>

        <div class="modal fade" id="largeModal" tabindex="-1">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Tambah {{ $title }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <form method="POST" enctype="multipart/form-data">
                  @csrf
                  <!-- General Form Elements -->
                
                    <div class="row mb-3">
                      <label for="inputText" class="col-sm-2 col-form-label">Nama</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama" value="{{ old('nama') }}">
                      </div>
                    </div>
                    <div class="row mb-3">
                      <label for="inputText" class="col-sm-2 col-form-label">Deskripsi</label>
                      <div class="col-sm-10">
                        <textarea name="deskripsi" id="" cols="72" rows="10">{{ old('deskripsi') }}</textarea>
                      </div>
                    </div>
                   
                    <div class="row mb-3">
                      <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="file" name="images[]" multiple>
                      </div>
                    </div>
                    <div class="row mb-3">
                      <label class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-10">
                        <select class="form-select" aria-label="Default select example" name="status">
                          <option>Pilih Status</option>
                          <option value="Aktif" value="{{ old('nama')=='Aktif'?'selected':'' }}">Aktif</option>
                          <option value="Tidak Aktif" value="{{ old('nama')=='Tidak Aktif'?'selected':'' }}">Tidak Aktif</option>
                        </select>
                      </div>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" name="add" value="add" class="btn btn-primary">Save changes</button>
                  </div>
                </form>
            </div>
          </div>
        </div><!-- End Large Modal-->

      </div>
    </div>
  </div>
  <div class="row">
    @if(!empty($b))
        
    @foreach ($b as $i)
        
    <div class="col-lg-4">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">{{ $i['nama'] }}</h5>
          <!-- Slides with fade transition -->
          <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <div class="carousel-inner">
              
              @foreach ($i['images'] as $no=>$a)
                  
              <div class="carousel-item {{ $no==0?'active':'' }}">
                <img src="{{ asset('images/benda sejarah/'.$a->image.' ') }}" class="d-block w-100" alt="...">
              </div>

              @endforeach
            </div>
      
            {{-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button> --}}
            <hr class="dropdown-divider">
            <span>{{ $i['deskripsi'] }}</span>
            <div>
              <a href="{{ URL::to('benda-sejarah-edit/'.$i['id'].'') }}" class="btn btn-primary" ><i class="bi bi-pencil-square"></i></a>
              <button onclick="del({{ $i['id'] }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button>
            </div>
          </div><!-- End Slides with fade transition -->
      
        </div>
      </div>
    </div>
    @endforeach
    @endif

  </div>
</section>


<!-- Basic Modal -->

<div class="modal fade" id="basicModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Apakah anda yakin menghapus ini?
      </div>
      <form method="POST" action="{{ route('del.benda_sejarah') }}">
        @csrf
        <input type="hidden" name="delId" id="delId">
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" name="del" value="del" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div><!-- End Basic Modal-->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
function del(id){
    var input = document.getElementById("delId");
    input.value = id;
    console.log(id);

}
</script>
@endsection