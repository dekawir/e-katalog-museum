@extends('layouts.index')
@section('content')
    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{{ $title }}</h5>
              <!-- Large Modal -->
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#largeModal">
                Tambah
              </button>

              <div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Tambah {{ $title }}</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <!-- General Form Elements -->
                    <form method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="row mb-3">
                        <label for="inputNumber" class="col-sm-2 col-form-label">File Upload</label>
                        <div class="col-sm-10">
                        <input class="form-control" type="file" name="images[]" multiple>
                        </div>
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" value="add" name="add">Save changes</button>
                    </div>
                  </form><!-- End General Form Elements -->

                  </div>
                </div>
              </div><!-- End Large Modal-->

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                      $no=1
                  @endphp
                  @foreach ($data as $i)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td> <img height="100" src="{{ asset('images/data temuan/'.$i->image.' ') }}" alt=""> </td>
                    <td>
                      <button onclick="del({{ $i->id }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>


    <!-- Basic Modal -->
   
    <div class="modal fade" id="basicModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Konfirmasi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Apakah anda yakin menghapus ini?
          </div>
          <form method="POST" action="{{ route('del.image') }}">
            @csrf
            <input type="hidden" name="delId" id="delId">
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" name="del" value="del" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- End Basic Modal-->

    
    <script>
        function del(id){
            var input = document.getElementById("delId");
            input.value = id;
            console.log(id);
    
        }
    </script>

@endsection