@extends('layouts.index')
@section('content')
    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{{ $title }}</h5>
              <!-- Large Modal -->
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#largeModal">
                Tambah
              </button>

              <div class="modal fade" id="largeModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Tambah {{ $title }}</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <!-- General Form Elements -->
                    <form method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="row mb-3">
                        <label class="col-sm-2 col-form-label">Lantai</label>
                        <div class="col-sm-10">
                          <select class="form-select" aria-label="Default select example" name="lantai">
                            <option selected>Pilih...</option>
                            <option value="1">Lantai 1</option>
                            <option value="2">Lantai 2</option>
                            <option value="3">Lantai 3</option>
                          </select>
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label class="col-sm-2 col-form-label">Ruang</label>
                        <div class="col-sm-10">
                          <select class="form-select" aria-label="Default select example" name="ruang">
                            <option selected>Pilih...</option>
                            <option value="Ruang Batu">Ruang Batu</option>
                            <option value="Ruang Keris">Ruang Keris</option>
                          </select>
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label class="col-sm-2 col-form-label">Benda Sejarah</label>
                        <div class="col-sm-10">
                          <select class="form-select" aria-label="Default select example" name="benda_sejarah">
                            <option selected>Pilih...</option>
                            @foreach ($temuan as $i)
                            <option {{ old('benda_sejarah')==$i->id ? 'selected':'' }} value="{{ $i->id }}">{{ $i->nama }}</option>
                            @endforeach
                        </select>
                        </div>
                      </div>

                      <div class="row mb-3">
                        <label  class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                          <textarea  cols="72" rows="5" name="deskripsi">{{ old('deskripsi') }}</textarea>
                        </div>
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" value="add" name="add">Save changes</button>
                    </div>
                  </form><!-- End General Form Elements -->

                  </div>
                </div>
              </div><!-- End Large Modal-->

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Benda</th>
                    <th scope="col">Lantai</th>
                    <th scope="col">Ruang</th>
                    <th scope="col">Penemu</th>
                    <th scope="col">Image</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                      $no=1
                  @endphp
                  @foreach ($data as $i)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $i->nama }}</td>
                    <td>{{ $i->lantai }}</td>
                    <td>{{ $i->ruang }}</td>
                    <td>{{ $i->penemu }}</td>
                    <td> <img height="100" src="{{ asset('images/data temuan/'.$i->image.' ') }}" alt=""> </td>
                    <td>
                      
                      <button onclick="editbtn({{ $i->id }})" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editLargeModal"><i class="bi bi-pencil-square"></i></button>
                      {{-- <a class="btn btn-warning" href="{{ URL::to('data-temuan-details/'.$i->id.' ') }}"><i class="bi bi-search"></i></a> --}}
                      <button onclick="del({{ $i->id }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button>
                     
                      {{-- <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a> --}}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>

    <div class="modal fade" id="editLargeModal" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Tambah {{ $title }}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
          <!-- General Form Elements -->
          <form method="POST">
            @csrf
            <input type="hidden" name="id" id="id">
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Lantai</label>
                <div class="col-sm-10">
                  <select class="form-select" aria-label="Default select example" name="lantai" id="lantai">
                    <option value="1">Lantai 1</option>
                    <option value="2">Lantai 2</option>
                    <option value="3">Lantai 3</option>
                  </select>
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Ruang</label>
                <div class="col-sm-10">
                  <select class="form-select" aria-label="Default select example" name="ruang" id="ruang">
                    <option value="Ruang Batu">Ruang Batu</option>
                    <option value="Ruang Keris">Ruang Keris</option>
                  </select>
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Benda Sejarah</label>
                <div class="col-sm-10">
                  <select class="form-select" aria-label="Default select example" name="benda_sejarah" id="benda_sejarah">
                    @foreach ($temuan as $i)
                    <option {{ old('benda_sejarah')==$i->id ? 'selected':'' }} value="{{ $i->id }}">{{ $i->nama }}</option>
                    @endforeach
                </select>
                </div>
              </div>

              <div class="row mb-3">
                <label  class="col-sm-2 col-form-label">Deskripsi</label>
                <div class="col-sm-10">
                  <textarea id="deskripsi" cols="72" rows="5" name="deskripsi">{{ old('deskripsi') }}</textarea>
                </div>
              </div>           

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" value="edit" name="edit">Save changes</button>
          </div>
        </form><!-- End General Form Elements -->

        </div>
      </div>
    </div><!-- End Large Modal-->


    <!-- Basic Modal -->
   
    <div class="modal fade" id="basicModal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Konfirmasi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Apakah anda yakin menghapus ini?
          </div>
          <form method="POST" action="{{ route('del.benda') }}">
            @csrf
            <input type="hidden" name="delId" id="delId">
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" name="del" value="del" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div><!-- End Basic Modal-->


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
      function editbtn(id){
          // var input = document.getElementById("editId");
          // input.value = id;
  
          // console.log(id)
          
          $.ajax({
              type: "GET",
              url: "/benda-sejarah-edit/"+id,
              success: function (response) {
                  console.log(response.data);
                  $('#id').val(response.data.id).change();
                  $('#lantai').val(response.data.lantai).change();
                  $('#ruang').val(response.data.ruang).change();
                  $('#benda_sejarah').val(response.data.id_data_temuan).change();
                  $('#deskripsi').val(response.data.deskripsi).change();
                 
              }
          });
      }
  </script>
  <script>
    function del(id){
        var input = document.getElementById("delId");
        input.value = id;
        console.log(id);

    }
</script>


@endsection