@extends('layouts.index')
@section('content')
    <section class="section">
      <div class="row">
        <div class="col-lg-12">

          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{{ $title }}</h5>
              {{-- <button class="btn btn-sucess" onclick="window.print()">Print this page</button> --}}
              <button type="button" onclick="window.print()" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#largeModal">
                Print
              </button>

              <!-- Table with stripped rows -->
              <table class="table datatable">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">No. Telp</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Tujuan</th>
                    <th scope="col">Tanggal Berkunjung</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @php
                      $no=1
                  @endphp
                  @foreach ($data as $i)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $i->nama }}</td>
                    <td>{{ $i->no_telp }}</td>
                    <td>{{ $i->alamat }}</td>
                    <td>{{ $i->tujuan }}</td>
                    <td>{{ date('d-F-Y',strtotime($i->tgl_berkunjung)) }}</td>
                    <td>{{ $i->status }}</td>
                    <td>
                      
                      <button onclick="editbtn({{ $i->id }})" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editLargeModal"><i class="bi bi-pencil-square"></i></button>
                      <button onclick="del({{ $i->id }})" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#basicModal"><i class="bi bi-trash"></i></button>
                      
                      {{-- <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a> --}}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
    </section>



@endsection