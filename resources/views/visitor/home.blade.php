@extends('visitor.index')
@push('hero')
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">

        <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
            <h1>Selamat Datang di Museum Geopark Batur</h1>
            <h2>Jalan Raya Penelokan, Kintamani, Bangli</h2>
            <div class="d-flex justify-content-center justify-content-lg-start">
                <a href="#about" class="btn-get-started scrollto">Tiket Online</a>
                <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
            </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
            <img src="{{ asset('images/batur.jpg') }}" class="img-fluid animated" alt="">
            </div>
        </div>
        </div>

    </section><!-- End Hero -->
@endpush
@section('content')
    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
        <div class="container" data-aos="fade-up">
  
          <div class="section-title">
            <h2>Benda Sejarah</h2>
          </div>

          <form method="post">
            @csrf
          <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for=""></label>
                    <input type="text" class="form-control" name="nama"  placeholder="Pencaharian..." value="{{ old('nama') }}">
                </div>
            </div>

            <div class="col-lg-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Lantai</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="lantai">
                        <option value="">Pilih..</option>
                      @foreach ($lantai as $j)
                      <option value="{{ $j->lantai }}">{{ $j->lantai }}</option>
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Ruangan</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="ruang">
                        <option value="">Pilih..</option>
                        @foreach ($ruang as $j)
                        <option value="{{ $j->ruang }}">{{ $j->ruang }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-auto mt-4">
                <button type="submit" name="cari" value="cari" class="btn btn-primary">Submit</button>
              </div>
          </div>
        </form>
          <hr class="mt-5">

          <div class="row">
            @if (!empty($benda))
                
            @foreach ($benda as $b)
            
            <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                <div class="icon-box" style="margin-top: 10px;">
                  <div class="">
                      <div class="portfolio-img"><img src="{{ asset('images/data temuan/'.$b->image.' ')}}" class="img-fluid" alt=""></div>
                    </div>
                    <h4><a href="">{{ $b->nama }}</a></h4>
                    <p>{{ $b->deskripsi }}</p>
                    <hr>
                    <a href="{{ URL::to('details/'.$b->id.' ') }}"><h6>Baca Selengkapnya</h6></a>
                  </div>
                </div>
                
              @endforeach
              @else
              <h4>Data tidak ditemukan</h4>
              @endif
          </div>
  
        </div>
      </section><!-- End Services Section -->
      
      <!-- ======= Cta Section ======= -->
      <section id="cta" class="cta">
        <div class="container" data-aos="zoom-in">
  
          <div class="row">
            <div class="col-lg-9 text-center text-lg-start">
              <h3>Tiket Online</h3>
              <p> Kini pemesanan tiket bisa lebih cepat dan mudah.</p>
            </div>
            <div class="col-lg-3 cta-btn-container text-center">
              <a class="cta-btn align-middle" href="/tiket-online">Tiket Online</a>
            </div>
          </div>
  
        </div>
      </section><!-- End Cta Section -->
  
      <!-- ======= Portfolio Section ======= -->
      <section id="portfolio" class="portfolio">
        <div class="container" data-aos="fade-up">
  
          <div class="section-title">
            <h2>Lantai Museum</h2>
            {{-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> --}}
          </div>
  
          <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
            <li data-filter="*" class="filter-active">Semua</li>
            @if(!empty($lantai))
            @foreach ($lantai as $i)
                
            <li data-filter=".filter-{{ $i->lantai }}">Lantai {{ $i->lantai }}</li>

            @endforeach
            @endif
          </ul>
  
          <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
            @if(!empty($data_lantai))
            @foreach ($data_lantai as $i)
            <div class="col-lg-4 col-md-6 portfolio-item filter-{{ $i->lantai }}">
              <div class="portfolio-img"><img src="{{ asset('images/data temuan/'.$i->image.' ')}}" class="img-fluid" alt=""></div>
              <div class="portfolio-info">
                <h4>{{ $i->nama }}</h4>
                <p>{{ $i->ruang }}</p>
                <a href="{{ asset('images/data temuan/'.$i->image.' ')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="{{ $i->nama }}"><i class="bx bx-search"></i></a>
                <a href="{{ URL::to('details/'.$i->id.' ') }}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
              </div>
            </div>
            @endforeach
            @endif
          </div>
  
        </div>
      </section><!-- End Portfolio Section -->
  
      {{-- <!-- ======= Pricing Section ======= -->
      <section id="pricing" class="pricing">
        <div class="container" data-aos="fade-up">
  
          <div class="section-title">
            <h2>Pricing</h2>
            <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
          </div>
  
          <div class="row">
  
            <div class="col-lg-4" data-aos="fade-up" data-aos-delay="100">
              <div class="box">
                <h3>Free Plan</h3>
                <h4><sup>$</sup>0<span>per month</span></h4>
                <ul>
                  <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
                  <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>
                  <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
                  <li class="na"><i class="bx bx-x"></i> <span>Pharetra massa massa ultricies</span></li>
                  <li class="na"><i class="bx bx-x"></i> <span>Massa ultricies mi quis hendrerit</span></li>
                </ul>
                <a href="#" class="buy-btn">Get Started</a>
              </div>
            </div>
  
            <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
              <div class="box featured">
                <h3>Business Plan</h3>
                <h4><sup>$</sup>29<span>per month</span></h4>
                <ul>
                  <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
                  <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>
                  <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
                  <li><i class="bx bx-check"></i> Pharetra massa massa ultricies</li>
                  <li><i class="bx bx-check"></i> Massa ultricies mi quis hendrerit</li>
                </ul>
                <a href="#" class="buy-btn">Get Started</a>
              </div>
            </div>
  
            <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
              <div class="box">
                <h3>Developer Plan</h3>
                <h4><sup>$</sup>49<span>per month</span></h4>
                <ul>
                  <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
                  <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>
                  <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
                  <li><i class="bx bx-check"></i> Pharetra massa massa ultricies</li>
                  <li><i class="bx bx-check"></i> Massa ultricies mi quis hendrerit</li>
                </ul>
                <a href="#" class="buy-btn">Get Started</a>
              </div>
            </div>
  
          </div>
  
        </div>
      </section><!-- End Pricing Section -->
   --}}
      {{-- <!-- ======= Contact Section ======= -->
      <section id="contact" class="contact">
        <div class="container" data-aos="fade-up">
  
          <div class="section-title">
            <h2>Contact</h2>
            <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
          </div>
  
          <div class="row">
  
            <div class="col-lg-5 d-flex align-items-stretch">
              <div class="info">
                <div class="address">
                  <i class="bi bi-geo-alt"></i>
                  <h4>Location:</h4>
                  <p>A108 Adam Street, New York, NY 535022</p>
                </div>
  
                <div class="email">
                  <i class="bi bi-envelope"></i>
                  <h4>Email:</h4>
                  <p>info@example.com</p>
                </div>
  
                <div class="phone">
                  <i class="bi bi-phone"></i>
                  <h4>Call:</h4>
                  <p>+1 5589 55488 55s</p>
                </div>
  
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
              </div>
  
            </div>
  
            <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
              <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="name">Your Name</label>
                    <input type="text" name="name" class="form-control" id="name" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="name">Your Email</label>
                    <input type="email" class="form-control" name="email" id="email" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="name">Subject</label>
                  <input type="text" class="form-control" name="subject" id="subject" required>
                </div>
                <div class="form-group">
                  <label for="name">Message</label>
                  <textarea class="form-control" name="message" rows="10" required></textarea>
                </div>
                <div class="my-3">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit">Send Message</button></div>
              </form>
            </div>
  
          </div>
  
        </div>
      </section><!-- End Contact Section --> --}}
@endsection