@extends('visitor.index')
@section('content')
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">
  
          <ol>
            <li><a href="/">Home</a></li>
            <li>{{ $title }}</li>
          </ol>
          <h2>{{ $title }}</h2>
  
        </div>
      </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
        <div class="container">
  
          <div class="row gy-4">
  
            <div class="col-lg-8">
              <div class="portfolio-details-slider swiper">
                <div class="swiper-wrapper align-items-center">
                    @foreach ($images as $i) 
                    <div class="swiper-slide">
                        <img src="{{ asset('images/data temuan/'.$i->image.' ')}}" alt="">
                    </div>
                    @endforeach
  
                </div>
                <div class="swiper-pagination"></div>
              </div>
            </div>
  
            @foreach ($data as $d)
                
            <div class="col-lg-4">
                <div class="portfolio-info">
                <h3>Informasi Benda Sejarah</h3>
                <ul>
                    <li><strong>Nama</strong>: {{ $d->nama }}</li>
                    <li><strong>Jenis Benda</strong>: {{ $d->jenis_benda }}</li>
                    <li><strong>Penemu</strong>: <{{ $d->penemu }}/li>
                    <li><strong>Tanggal Ditemukan</strong>: {{ $d->tgl_ditemukan }}</li>
                    <li><strong>Lokasi</strong>: {{ $d->lokasi }}</li>
                    <li><strong>Ruang</strong>: {{ $d->ruang }}</li>
                    <li><strong>Lantai</strong>: {{ $d->lantai }}</li>
                </ul>
            </div>
            <div class="portfolio-description">
                <h2>Deskripsi</h2>
                <p>{{ $d->deskripsi }} </p>
            </div>
        </div>
        @endforeach
  
          </div>
  
        </div>
    </section><!-- End Portfolio Details Section -->
  
@endsection