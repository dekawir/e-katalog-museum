<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataTemuan extends Model
{
    use HasFactory;
    protected $table='datatemuan';
    protected $guarded = ['id'];
}
