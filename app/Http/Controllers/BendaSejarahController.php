<?php

namespace App\Http\Controllers;

use App\Models\BendaSejarah;
use App\Models\DataTemuan;
use App\Models\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\File;

class BendaSejarahController extends Controller
{
    public function index(Request $request)
    {
        if($request->add){
           
            $request->validate([
                'lantai'=> 'required',
                'ruang'=> 'required',
                'benda_sejarah'=> 'required|unique:App\Models\BendaSejarah,id_data_temuan',
                'deskripsi'=> 'required',
            ]);
           
            $data = [
                'lantai'=> $request->lantai,
                'ruang'=> $request->ruang,
                'id_data_temuan'=> $request->benda_sejarah,
                'deskripsi'=> $request->deskripsi,
            ];

            BendaSejarah::create($data);

            return back()->with('success','Data Berhasil Ditambah'); 
        } 

        if($request->edit){
           
            $request->validate([
                'lantai'=> 'required',
                'ruang'=> 'required',
                'benda_sejarah'=> 'required',
                'deskripsi'=> 'required',
            ]);
           
            $data = [
                'lantai'=> $request->lantai,
                'ruang'=> $request->ruang,
                'id_data_temuan'=> $request->benda_sejarah,
                'deskripsi'=> $request->deskripsi,
            ];

            BendaSejarah::where('id',$request->id)->update($data);

            return back()->with('success','Data Berhasil Diubah'); 
        }                
        
        DB::statement("SET SQL_MODE=''");
        return view('benda-sejarah',[
            'title'=>'Benda Sejarah',
            'data'=>DB::select('select bendasejarah.*, datatemuan.nama, datatemuan.penemu, images.image from bendasejarah inner join datatemuan on bendasejarah.id_data_temuan=datatemuan.id inner join images on datatemuan.id=images.id_data_temuan group by datatemuan.id'),
            'temuan' => DataTemuan::all()
        ]);
    }

    public function edit($id)
    {
        $data = BendaSejarah::find($id);
        return response()->json([
            'status'=>200,
            'data'=> $data,
        ]);
    }

    public function destroy(Request $request)
    {
        BendaSejarah::find($request->delId)->delete();
    
        return back()->with('success','Data berhasil dihapus');
    }

}
