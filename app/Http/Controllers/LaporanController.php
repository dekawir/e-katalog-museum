<?php

namespace App\Http\Controllers;

use App\Models\DataBukuTamu;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index()
    {
        return view('laporan',[
            'title'=>'Laporan',
            'data'=>DataBukuTamu::where('status','ACC')->get()
        ]);
    }
}
