<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(Request $request)
    {
        if ($user = Auth::user()) {
            if ($user->level == 's_admin') {
                return redirect()->intended('dashboard');
            } elseif ($user->level == 'admin') {
                return redirect()->intended('/');
            }
        }
        
        if($request->post())
        {
            request()->validate([
                'email' => 'required',
                'password' => 'required',
            ]);

        $kredensil = $request->only('email','password');

            if (Auth::attempt($kredensil)) {
                $user = Auth::user();
                if ($user->level == 's_admin') {
                    return redirect()->intended('dashboard')->with('info','Selamat Datang');
                } elseif ($user->level == 'admin') {
                    return redirect()->intended('/');
                }
                return redirect()->intended('/login');

                // return redirect()->intended('dashboard')->with('info','Selamat Datang');
            }

        return redirect('login')->withInput()->with('error','Login failed, Please check email & password');

        }
        return view('login',[
            'title'=>'Login'
        ]);
    }

    public function register(Request $request)
    {
        if($request->reg){
            request()->validate([
                'nama' => 'required',
                'no_telp' => 'required',
                'alamat' => 'required',
                'email' => 'required|unique:users,email',
                'password' => 'required',
            ]);

            $data = [
                'nama'=>$request->nama,
                'no_telp'=>$request->no_telp,
                'alamat'=>$request->alamat,
                'level'=>'admin',
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
            ];

            User::create($data);
            return $this->index($request);
        }
        return view('register',[
            'title'=>'Register'
        ]);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('login');
    }
}
