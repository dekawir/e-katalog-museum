<?php

namespace App\Http\Controllers;

use App\Models\KritikSaran;
use Illuminate\Http\Request;

class KritikSaranController extends Controller
{
    public function index(Request $request)
    {
        return view('kritik-saran',[
            'title'=>'Kritik & Saran',
            'data'=> KritikSaran::all()
        ]);
    }
}
