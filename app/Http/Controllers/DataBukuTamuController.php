<?php

namespace App\Http\Controllers;

use App\Models\DataBukuTamu;
use Illuminate\Http\Request;

class DataBukuTamuController extends Controller
{
    public function index(Request $request)
    {
        if($request->add){
           
            $request->validate([
                'nama'=> 'required',
                'no_telp'=> 'required|max:16',
                'alamat'=> 'required',
                'tujuan'=> 'required',
                'tgl_berkunjung'=> 'required|after:today',
                'status'=> 'required',
            ]);
           
            $data = [
                'nama'=> $request->nama,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
                'tujuan'=> $request->tujuan,
                'tgl_berkunjung'=> $request->tgl_berkunjung,
                'status'=> $request->status,
            ];

            DataBukuTamu::create($data);

            return back()->with('success','Data Berhasil Ditambah'); 
        }

        if($request->edit){
           
            $request->validate([
                'nama'=> 'required',
                'no_telp'=> 'required|max:16',
                'alamat'=> 'required',
                'tujuan'=> 'required',
                'tgl_berkunjung'=> 'required|after:today',
                'status'=> 'required',
            ]);
           
            $data = [
                'nama'=> $request->nama,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
                'tujuan'=> $request->tujuan,
                'tgl_berkunjung'=> $request->tgl_berkunjung,
                'status'=> $request->status,
            ];

            DataBukuTamu::where('id',$request->id)->update($data);

            return back()->with('success','Data Berhasil Diubah'); 
        }
        return view('data-buku-tamu',[
            'title'=>'Buku Tamu',
            'data'=> DataBukuTamu::all()
        ]);
    }

    public function edit($id)
    {
        $data = DataBukuTamu::find($id);
        return response()->json([
            'status'=>200,
            'data'=> $data,
        ]);
    }

    public function destroy(Request $request)
    {
        DataBukuTamu::find($request->delId)->delete();

        return back()->with('success','Data berhasil dihapus');
    }
}
