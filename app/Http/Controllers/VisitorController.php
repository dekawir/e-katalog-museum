<?php

namespace App\Http\Controllers;

use App\Models\DataJadwal;
use App\Models\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VisitorController extends Controller
{
    public function index(Request $request)
    {
    
        
        DB::statement("SET SQL_MODE=''");
        if($request->cari){
            
            if(!empty($request->nama)){
                $nama = "where datatemuan.nama like '%$request->nama%' ";
            }else{
                $nama='';
            }
            
            if(!empty($request->lantai)){
                $lantai = "and bendasejarah.lantai like '%$request->lantai%' ";
            }else{
                $lantai='';
            }

            if(!empty($request->ruang)){
                $ruang = "and bendasejarah.ruang like '%$request->ruang%' ";
            }else{
                $ruang='';
            }

            $benda = DB::select('select bendasejarah.*, datatemuan.nama, datatemuan.penemu, images.image from bendasejarah 
            inner join datatemuan on bendasejarah.id_data_temuan=datatemuan.id 
            inner join images on datatemuan.id=images.id_data_temuan '.$nama.' '.$lantai.' '.$ruang.' group by datatemuan.id  ');
           
            return view('visitor.home',[
                'title'=>'Landing Page',
                'lantai'=> DB::select('select lantai from bendasejarah group by lantai'),
                'ruang'=> DB::select('select ruang from bendasejarah group by ruang'),
                'benda'=> $benda,
                'lantai'=> DB::select('select lantai from bendasejarah group by lantai'),
                'data_lantai'=>DB::select('select bendasejarah.*, datatemuan.nama, datatemuan.penemu, images.image from bendasejarah inner join datatemuan on bendasejarah.id_data_temuan=datatemuan.id inner join images on datatemuan.id=images.id_data_temuan group by datatemuan.id'),


    
            ]);
        }
        
        return view('visitor.home',[
            'title'=>'Home',
            'lantai'=> DB::select('select lantai from bendasejarah group by lantai'),
                'ruang'=> DB::select('select ruang from bendasejarah group by ruang'),
            'lantai'=> DB::select('select lantai from bendasejarah group by lantai'),
            'benda'=>DB::select('select bendasejarah.*, datatemuan.nama, datatemuan.penemu, images.image from bendasejarah inner join datatemuan on bendasejarah.id_data_temuan=datatemuan.id inner join images on datatemuan.id=images.id_data_temuan group by datatemuan.id'),
            'data_lantai'=>DB::select('select bendasejarah.*, datatemuan.nama, datatemuan.penemu, images.image from bendasejarah inner join datatemuan on bendasejarah.id_data_temuan=datatemuan.id inner join images on datatemuan.id=images.id_data_temuan group by datatemuan.id'),
            

        ]);
    }

    public function details($id)
    {
        DB::statement("SET SQL_MODE=''");

        $benda = DB::select('select * from bendasejarah 
            inner join datatemuan on bendasejarah.id_data_temuan=datatemuan.id 
            inner join images on datatemuan.id=images.id_data_temuan where bendasejarah.id='.$id.' group by datatemuan.id');
        
        $images = Images::where('id_data_temuan',$benda[0]->id_data_temuan)->get();
        return view('visitor.details',[
            'title'=>'Informasi Benda Sejarah',
            'data'=> $benda,
            'images'=> $images,
        ]);
    }

    public function tiket(Request $request)
    {
        
        if($request->pesan){
            if(!empty(Auth::user()->nama)){
                $request->validate([
                    'tanggal'=> 'required|after:today',
                    'tujuan'=> 'required',
                ]);

                $data = [
                    'nama'=> Auth::user()->nama,
                    'no_telp'=> Auth::user()->no_telp,
                    'alamat'=> Auth::user()->alamat,
                    'tgl_berkunjung'=>$request->tanggal,
                    'tujuan'=>$request->tujuan,
                ];

                DataJadwal::create($data);
                return back()->with('sucess','Terima Kasih sudah memesan tiket');
            }else{
                return redirect('/login')->with('error','Maaf, Anda belum login');
            }
            

           
        }

        return view('visitor.tiket-online',[
            'title'=>'Tiket Online',
        ]);
    }
}
