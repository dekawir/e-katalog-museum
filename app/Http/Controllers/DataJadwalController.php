<?php

namespace App\Http\Controllers;

use App\Models\DataJadwal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DataJadwalController extends Controller
{
    public function index(Request $request)
    {
        if($request->add){
           
            $request->validate([
                'nama'=> 'required',
                'no_telp'=> 'required|max:16',
                'alamat'=> 'required',
                'tujuan'=> 'required',
                'tgl_berkunjung'=> 'required|after:today',
            ]);
           
            $data = [
                'nama'=> $request->nama,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
                'tujuan'=> $request->tujuan,
                'tgl_berkunjung'=> $request->tgl_berkunjung,
            ];

            DataJadwal::create($data);

            return back()->with('success','Data Berhasil Ditambah'); 
        }

        if($request->edit){
           
            $request->validate([
                'nama'=> 'required',
                'no_telp'=> 'required|max:16',
                'alamat'=> 'required',
                'tujuan'=> 'required',
                'tgl_berkunjung'=> 'required|after:today',
            ]);
           
            $data = [
                'nama'=> $request->nama,
                'no_telp'=> $request->no_telp,
                'alamat'=> $request->alamat,
                'tujuan'=> $request->tujuan,
                'tgl_berkunjung'=> $request->tgl_berkunjung,
            ];

            DataJadwal::where('id',$request->id)->update($data);

            return back()->with('success','Data Berhasil Diubah'); 
        }
        

        return view('jadwal-kunjungan',[
            'title'=>'Jadwal Kunjungan',
            'data'=> DataJadwal::all(),
        ]);
    }
    public function edit($id)
    {
        $data = DataJadwal::find($id);
        return response()->json([
            'status'=>200,
            'data'=> $data,
        ]);
    }

    public function destroy(Request $request)
    {
        DataJadwal::find($request->delId)->delete();
        return back()->with('success','Data berhasil dihapus');
    }
}
