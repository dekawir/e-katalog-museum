<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if($request->add){
            $validator = Validator::make($request->all(),[
                'nama'=>'required',
                'email'=>'required|unique:users',
                'password'=>'required',
                'alamat'=>'required',
                'no_telp'=>'required',
                
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $data = [
                'nama'=>$request->nama,
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'alamat'=>$request->alamat,
                'no_telp'=>$request->no_telp,
                'level'=>'admin',
            ];
           
            User::create($data);
            return back()->with('success','Data berhasil ditambah');

        }

        if($request->edit){
            $validator = Validator::make($request->all(),[
                'nama'=>'required',
                'email'=>'required',
                'alamat'=>'required',
                'no_telp'=>'required',
                
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            if(empty($request->password)){
                $data = [
                    'nama'=>$request->nama,
                    'email'=>$request->email,
                    'alamat'=>$request->alamat,
                    'no_telp'=>$request->no_telp,
                    'level'=>'admin',
                ];
            }else{
                $data = [
                    'nama'=>$request->nama,
                    'email'=>$request->email,
                    'password'=>bcrypt($request->password),
                    'alamat'=>$request->alamat,
                    'no_telp'=>$request->no_telp,
                    'level'=>'admin',
                ];
            }
           
            User::where('id',$request->id)->update($data);
            return back()->with('success','Data berhasil diubah');

        }

        return view('data-user',[
            'title'=>'Data User',
            'data'=>User::where('level','admin')->get()
        ]);
    }

    public function edit($id)
    {
        $data = User::find($id);
        return response()->json([
            'status'=>200,
            'data'=> $data,
        ]);
    }

    public function destroy(Request $request)
    {
        User::find($request->delId)->delete();
        return back()->with('success','Data berhasil dihapus');
    }
}
