<?php

namespace App\Http\Controllers;

use App\Models\DataTemuan;
use App\Models\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class DataTemuanController extends Controller
{
    public function index(Request $request)
    {
        if($request->add){
            // $validator = Validator::make($request->all(),[
            $request->validate([

                'nama'=>'required',
                'penemu'=>'required',
                'jenis_benda'=>'required',
                'tgl_ditemukan'=>'required|date|before:today',
                'lokasi'=>'required',
                'images' => 'required',
                'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                
            ]);

            $images = [];
            if ($request->images){
                foreach($request->images as $key => $image)
                {
                    $imageName = time().rand(1,99).'.'.$image->extension();  
                    $image->move(public_path('images/data temuan'), $imageName);
      
                    $images[]['name'] = $imageName;
                }
            }

            

            $data = [
                'nama'=>$request->nama,
                'penemu'=>$request->penemu,
                'jenis_benda'=>$request->jenis_benda,
                'tgl_ditemukan'=>$request->tgl_ditemukan,
                'lokasi'=>$request->lokasi,
                // 'image'=>$imageName,
            ];
          
            DataTemuan::create($data);

            if(DataTemuan::count()>0){
                $getId = DB::table('datatemuan')->select('id')->latest('id')->first();
                $datatemuan = $getId->id;
            }else{
                $datatemuan=0;
            }

            $id=$datatemuan;

            foreach ($images as $key => $image) {
                $dataImage = [
                    'id_data_temuan'=> $id,
                    'image'=>$image['name']
                ];
                Images::create($dataImage);
            }
            return back()->with('success','Data berhasil ditambah');

        }

        if($request->edit){
            // $validator = Validator::make($request->all(),[
            $request->validate([
                'nama'=>'required',
                'penemu'=>'required',
                'jenis_benda'=>'required',
                'tgl_ditemukan'=>'required',
                'lokasi'=>'required',
            ]);
            
            $data = [
                'nama'=>$request->nama,
                'penemu'=>$request->penemu,
                'jenis_benda'=>$request->jenis_benda,
                'tgl_ditemukan'=>$request->tgl_ditemukan,
                'lokasi'=>$request->lokasi,
            ];
          
            DataTemuan::where('id',$request->id)->update($data);
            return back()->with('success','Data berhasil ditambah');

        }
        DB::statement("SET SQL_MODE=''");
        return view('data-temuan',[
            'title'=>'Data Temuan',
            'data'=> DB::select('select datatemuan.*, images.image from datatemuan inner join images on datatemuan.id=images.id_data_temuan group by datatemuan.id')
        ]);
    }

    public function details(Request $request, $id)
    {
        if($request->add){
            // $validator = Validator::make($request->all(),[
            $request->validate([
                'images' => 'required',
                'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                
            ]);

            $images = [];
            if ($request->images){
                foreach($request->images as $key => $image)
                {
                    $imageName = time().rand(1,99).'.'.$image->extension();  
                    $image->move(public_path('images/data temuan'), $imageName);
      
                    $images[]['name'] = $imageName;
                }
            }         
           
            foreach ($images as $key => $image) {
                $dataImage = [
                    'id_data_temuan'=> $id,
                    'image'=>$image['name']
                ];
                Images::create($dataImage);
            }
            return back()->with('success','Data berhasil ditambah');

        }
        return view('data-temuan-details',[
            'title'=>'Data Temuan',
            'data'=> Images::where('id_data_temuan',$id)->get()
        ]);
    }
    public function edit($id)
    {
        $data = DataTemuan::find($id);
        return response()->json([
            'status'=>200,
            'data'=> $data,
        ]);
    }

    public function destroy(Request $request)
    {
        DataTemuan::find($request->delId)->delete();
        Images::where('id_data_temuan',$request->delId)->delete();

        return back()->with('success','Data berhasil dihapus');
    }

    public function image_destroy(Request $request)
    {
        // dd($request->delId);
        Images::where('id',$request->delId)->delete();
        return back()->with('success','Data berhasil dihapus');
    }
}
