<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = [
            'nama'=>'Admin',
            'email'=>'admin@e-katalog.com',
            'password'=>bcrypt('admin12345'),
            'no_telp'=>'085792355688',
            'alamat'=>'Gunung Batur',
            'level'=>'s_admin',
        ];

        User::create($user);
    }
}
