<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('datatemuan', function (Blueprint $table) {
            $table->id();
            $table->string('nama',50);
            $table->string('penemu',50);
            $table->string('jenis_benda',50);
            $table->date('tgl_ditemukan');
            $table->string('lokasi',255);
            // $table->string('image',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('datatemuan');
    }
};
