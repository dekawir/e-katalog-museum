<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BendaSejarahController;
use App\Http\Controllers\DataBukuTamuController;
use App\Http\Controllers\DataJadwalController;
use App\Http\Controllers\DataPengunjungController;
use App\Http\Controllers\DataTemuanController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KritikSaranController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VisitorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard');
// });

Route::get('/',[VisitorController::class,'index']);
Route::post('/',[VisitorController::class,'index']);
Route::get('/details/{id}',[VisitorController::class,'details']);
Route::get('/tiket-online',[VisitorController::class,'tiket']);
Route::post('/tiket-online',[VisitorController::class,'tiket']);

Route::get('/login',[AuthController::class, 'index']);
Route::post('/login',[AuthController::class, 'index'])->name('login');

Route::get('/register',[AuthController::class, 'register']);
Route::post('/register',[AuthController::class, 'register']);

Route::group(['middleware' => ['auth']], function () {
    Route::get('logout',[AuthController::class, 'logout'])->name('logout');

    Route::get('/dashboard',[HomeController::class,'index']);
    
    Route::get('/data-user',[UserController::class,'index']);
    Route::post('/data-user',[UserController::class,'index']);
    Route::get('/data-user-edit/{id}',[UserController::class,'edit']);
    Route::post('/data-user-del',[UserController::class,'destroy'])->name('del.user');

    Route::get('/data-temuan',[DataTemuanController::class,'index']);
    Route::post('/data-temuan',[DataTemuanController::class,'index']);
    Route::get('/data-temuan-edit/{id}',[DataTemuanController::class,'edit']);
    Route::get('/data-temuan-details/{id}',[DataTemuanController::class,'details']);
    Route::post('/data-temuan-details/{id}',[DataTemuanController::class,'details']);
    Route::post('/data-temuan-del',[DataTemuanController::class,'destroy'])->name('del.temuan');
    Route::post('/data-temuan-gambar-del',[DataTemuanController::class,'image_destroy'])->name('del.image');

    Route::get('/data-pengunjung',[DataPengunjungController::class,'index']);
    Route::post('/data-pengunjung',[DataPengunjungController::class,'index']);
    Route::get('/data-pengunjung-edit/{id}',[DataPengunjungController::class,'edit']);
    Route::post('/data-pengunjung-del',[DataPengunjungController::class,'destroy'])->name('del.pengunjung');
    
    Route::get('/benda-sejarah',[BendaSejarahController::class,'index']);
    Route::post('/benda-sejarah',[BendaSejarahController::class,'index']);
    Route::get('/benda-sejarah-edit/{id}',[BendaSejarahController::class,'edit']);
    Route::post('/benda-sejarah-edit/{id}',[BendaSejarahController::class,'edit']);
    Route::get('/benda-sejarah-show/{id}',[BendaSejarahController::class,'show']);
    Route::post('/benda-sejarah-del',[BendaSejarahController::class,'destroy'])->name('del.benda');
    
    
    Route::get('/laporan',[LaporanController::class,'index']);
    
    Route::get('/buku-tamu',[DataBukuTamuController::class,'index']);
    Route::post('/buku-tamu',[DataBukuTamuController::class,'index']);
    Route::get('/buku-tamu-edit/{id}',[DataBukuTamuController::class,'edit']);
    Route::post('/buku-tamu-del',[DataBukuTamuController::class,'destroy'])->name('del.buku_tamu');
    
    Route::get('/jadwal-kunjungan',[DataJadwalController::class,'index']);
    Route::post('/jadwal-kunjungan',[DataJadwalController::class,'index']);
    Route::get('/jadwal-kunjungan-edit/{id}',[DataJadwalController::class,'edit']);
    Route::post('/jadwal-kunjungan-del',[DataJadwalController::class,'destroy'])->name('del.jadwal');

});
